TESTS := $(patsubst %/,%,$(wildcard test/*/))
$(info tests: $(TESTS))
BAGA_MK := $(abspath baga.mk)

.PHONY: default all tests clean $(TESTS)
default: tests
all: tests wtest

tests: $(TESTS)

clean:
	rm -rf _out _temp

$(TESTS):
	$(MAKE) -f $(BAGA_MK) -C $@ all

wtest:
	mkdir -p _temp
	-rm -f _temp/baga.7z
	7z a -bd -bb0 -r _temp/baga.7z baga.mk makefile test > /dev/null
	wdevcp _temp/baga.7z '%TEMP%\baga.7z'
	wdevcp wtest.cmd '%TEMP%\baga-wtest.cmd'
	wdev '%TEMP%\baga-wtest.cmd %TEMP%'

PREFIX_DIR ?= $(HOME)/.local

define baga_sh_content
#!/bin/sh
make -f $(abspath $(PREFIX_DIR))/lib/baga/baga.mk --no-print-directory -sj8 "$$@"
endef

define vbaga_sh_content
#!/bin/sh
make -f $(abspath $(PREFIX_DIR))/lib/baga/baga.mk --no-print-directory cmd_pfx= "$$@"
endef

install:
	install -m444 -D -t $(PREFIX_DIR)/lib/baga baga.mk
	mkdir -p $(PREFIX_DIR)/bin
	$(file > $(PREFIX_DIR)/bin/baga,$(baga_sh_content))
	$(file > $(PREFIX_DIR)/bin/vbaga,$(vbaga_sh_content))
	chmod 755 $(PREFIX_DIR)/bin/baga $(PREFIX_DIR)/bin/vbaga

