# generic GNU make utilities {{{1
invoked_makefile := $(lastword $(MAKEFILE_LIST))

empty :=
space := $(empty) $(empty)
comma := ,
backslash := \\

define enter


endef

UALNUM_CHARS = a b c d e f g h i j k l m n o p q r s t u v w x y z A B C D E F G H I J K L M N O P Q R S T U V W X Y Z 0 1 2 3 4 5 6 7 8 9 0 _ -
COUNTER := 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59 60 61 62 63 64 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 81 82 83 84 85 86 87 88 89 90 91 92 93 94 95 96 97 98 99 100 101 102 103 104 105 106 107 108 109 110 111 112 113 114 115 116 117 118 119 120 121 122 123 124 125 126 127 128
counter_minus_1 = $(word $1,0 $(COUNTER))
ualnum_break_0 = $1
$(foreach i,$(wordlist 1,$(words $(UALNUM_CHARS)),$(COUNTER)),$(eval ualnum_break_$i = $$(subst $$(word $i,$$(UALNUM_CHARS)),$$(word $i,$$(UALNUM_CHARS)) ,$$(call ualnum_break_$$(call counter_minus_1,$i),$$1))))
ualnum_break = $(call ualnum_break_$(words $(UALNUM_CHARS)),$1)
ualnum_len = $(words $(call ualnum_break,$1))
ualnum_tpad = $(subst , ,$(subst $(space),,$(wordlist 1,$2,$(call ualnum_break,$1) $(foreach i,$(wordlist 1,$2,$(COUNTER)),))))

if_equal = $(if $(and $(findstring $1,$2),$(findstring $2,$1),y),$3,$4)

init_var = $(if $(filter undefined,$(origin $1)),$(eval $1 := $2))
lazy_init_var = $(if $(filter undefined,$(origin $1)),$(eval $1 = $2))

define rule_cmd

	$1
endef

ifneq ($(filter 1 2,$(verbose)),)
vmsg = $(info $1)
#vvar = $(info $(subst $(enter)$(space),$(enter),$(foreach v,$1,$2$v: >>>$($v)<<<$(enter))))
vvar = $(info $(subst $(enter)$(enter),,$(enter)$(foreach v,$1,$(enter)$2$v: >>>$($v)<<<)))
vtvar = $(info $(subst $(enter)$(enter),,$(enter)$(foreach v,$1,$(enter)$2$v: >>>$(value $v)<<<)))
else
vmsg =
vvar =
endif

ifeq ($(verbose),2)
vvmsg = $(info $1)
vvvar = $(info $(subst $(enter)$(enter),,$(enter)$(foreach v,$1,$(enter)$2$v: >>>$($v)<<<)))
else
vvmsg =
vvvar =
endif

ifeq ($(OS),Windows_NT)
host_is_windows := 1
undefine host_is_unixlike
else
undefine host_is_windows
host_is_unixlike := 1
endif

# Windows host platform utils
ifneq ($(host_is_windows),)
unix_path = $(subst \,/,$1)
is_abs_path_a = $(if $(findstring :/,$1)$(filter //%,$1),1,)
is_abs_path = $(call is_abs_path_a,$(call unix_path,$1))
abs_path_s = $(if $(word 2,$(subst :, ,$1)),$(firstword $(subst :, ,$1)):$(abspath $(lastword $(subst :, ,$1))),$(abspath $1))
abs_path = $(call abs_path_s,$(subst \,/,$1))
host_path = $(subst /,\,$1)
make_dir = cmd /c IF NOT EXIST $(call hnts_path,$1) MD $(call hnts_path,$1)
rm_dir = cmd /c RMDIR $(call hnts_path,$1)
rm_tree = cmd /c RMDIR /S /Q $(call hnts_path,$1)
rm_file = cmd /c ERASE /F $(call host_path,$1)
mv_file = cmd /c MOVE /Y $(call host_path,$1) $(call host_path,$2)
cp_file = cmd /c COPY /Y $(call host_path,$1) $(call host_path,$2) > NUL
user_name := $(USERNAME)
host_name := $(COMPUTERNAME)
default_config_dir := $(APPDATA)
export_inc = copy /Y $(call host_path,$1) $(call hnts_path,$2)
echo = @echo ^$1

endif

ifneq ($(host_is_unixlike),)
# Unix-like host platform utils
is_abs_path = $(if $(filter /%,$1),1)
abs_path = $(abspath $1)
host_path = $1
unix_path = $1
make_dir = mkdir -p $(call hnts_path,$1)
rm_dir = rmdir $(call hnts_path,$1)
rm_tree = rm -rf $1
rm_file = rm -f $1
mv_file = mv -f $1 $2
cp_file = cp -f $1 $2
user_name := $(USER)
host_name := $(shell hostname)
default_config_dir := $(HOME)/.config
export_inc = install -m 444 -T $1 $2
echo = @echo "$(subst ",\\",$1)"
endif

# unix no-trailing-slash path
unts_path = $(patsubst %/,%,$(call unix_path,$1))

# unix append-trailing-slash path
# if $1 is empty it will stay empty
uats_path = $(addsuffix /,$(call unts_path,$1))

hnts_path = $(call host_path,$(call unts_path,$1))

# expands words with "*" wildcard and keeps the words without "*"
expand_wildcard = $(sort $(foreach f,$1,$(if $(findstring *,$f),,$f)) $(wildcard $(foreach f,$1,$(if $(findstring **,$f),$(foreach m,* */* */*/* */*/*/* */*/*/*/*,$(subst **,$m,$f)),$(and $(findstring *,$f),$f)))))

# combine_path,$1=path,$2=dir
combine_path = $(patsubst %/.,%,$(patsubst ./%,%,$(foreach p,$1,$(if $(call is_abs_path,$p),$p,$(call uats_path,$2)$p))))

# end generic GNU make utilities }}}

cmd_pfx := @
WS_LOOKUP_DIR_LIST := ./ ../ ../ ../../ ../../../ ../../../../
WS_FILE_SUFFIX := .baga-ws.mk
WS_FILE_DEFAULT_NAME := baga-ws.mk
PROJECT_FILE_SUFFIX := .baga-proj.mk
USE_SLIB_SUFFIX := .baga-slib.mk
USE_DLIB_SUFFIX := .baga-dlib.mk
PRODUCTS := slib dlib exe
SUB_MAKE_FLAGS := --no-print-directory
#SUB_MAKE_FLAGS := -s

PROFILE_VARS := \
	out_inc_sub_dir out_slib_sub_dir out_dlib_sub_dir out_exe_sub_dir \
	obj_suffix \
	slib_prefix slib_suffix \
	dlib_prefix dlib_suffix \
	exe_prefix exe_suffix \
	opt_cpp_include opt_cpp_define opt_use_lib_file \
	cpp_includes c_includes cxx_includes \
	tags lib_dep_dirs quick

PROFILE_RULES := \
	slib_export \
	dlib_export \
	exe_export \
	cc_dep \
	msvc_dep

PROJECT_VARS := \
	product \
	name \
	lib_deps \
	project_deps \
	project_base_dir \
	temp_dir out_dir \
	c.sources cxx.sources \
	pub_inc_dir pub_inc_files \
	c_flags cpp_flags cxx_flags ld_flags ld_libs ld_sys_libs \
	user_ld_flags user_ld_libs \
	src_to_obj_types \
	want.debug_info \
	custom_rules

SRC_VARS_WITH_PROJECT_FILES := dep
SRC_VARS.c := cpp_flags c_flags
SRC_VARS.cxx := cpp_flags cxx_flags

baga_mode := workspace

ifeq ($(baga_mode),workspace)
.PHONY: quick
quick:

# helpers to be used in .baga-proj.mk files
c_flags.lto_on_release = $(if $(and $(release),$(if $(no_lto),,x)),$(if $(cc),-flto)$(if $(msvc),/GL))
cxx_flags.lto_on_release = $(if $(and $(release),$(if $(no_lto),,x)),$(if $(cc),-flto)$(if $(msvc),/GL))
ld_flags.lto_on_release = $(if $(and $(release),$(if $(no_lto),,x)),$(if $(cc),-flto)$(if $(msvc),/LTCG))

c_cxx_flags.no_opt = \
	$(if $(msvc),/Od) \
	$(if $(cc),-O0)

c_cxx_flags.full_opt = \
	$(if $(msvc),/Ox) \
	$(if $(cc),$(if $(or $(gcc),$(clang)),-Ofast,-O2))

c_cxx_flags.no_opt_on_debug = $(if $(debug), $(c_cxx_flags.no_opt))
c_cxx_flags.full_opt_on_release = $(if $(release), $(c_cxx_flags.full_opt))

c_cxx_flags.config_optimizations = \
	$(c_cxx_flags.no_opt_on_debug) \
	$(c_cxx_flags.full_opt_on_release) \
	$(c_cxx_flags.lto_on_release)

c_flags.config_optimizations = $(c_cxx_flags.config_optimizations)
cxx_flags.config_optimizations = $(c_cxx_flags.config_optimizations)

c_cxx_flags.all_warnings = \
	$(if $(cc),-Wall -Wextra -Wpedantic) \
	$(if $(msvc),/Wall)

c_cxx_flags.warnings_as_errors = \
	$(if $(cc),-Werror) \
	$(if $(msvc),/WX)

c_cxx_flags.all_warnings_as_errors = \
	$(c_cxx_flags.all_warnings) \
	$(c_cxx_flags.warnings_as_errors)
c_flags.all_warnings_as_errors = $(c_cxx_flags.all_warnings_as_errors)
cxx_flags.all_warnings_as_errors = $(c_cxx_flags.all_warnings_as_errors)

c_cxx_flags.pic = $(if $(cc),$(if $(and $(clang),$(mingw)),,-fPIC))
c_flags.pic = $(c_cxx_flags.pic)
cxx_flags.pic = $(c_cxx_flags.pic)

# this is added automatically to c/cxx-flags and is controlled by want.debug_info var
c_cxx_flags.rule.debug_info = $(if $(want.debug_info.$2.$1),$(if $(tag.cc.$2),-g)$(if $(tag.msvc.$2),/Z7))

ld_flags.rule.debug_info = $(if $(want.debug_info.$2.$1),$(if $(tag.cc.$2),-g)$(if $(tag.msvc.$2),/debug /pdb:$(call host_path,$(temp_dir.$2.$1)/$1.pdb)))

HL_VARS := \
	HL_PRJ_MK_TEXT HL_PRJ_MK_CMD HL_PRJ_MK_PROJECT HL_PRJ_MK_PROFILE HL_PRJ_MK_FILE \
	HL_CC_TEXT HL_CC_CMD HL_CC_PROJECT HL_CC_PROFILE HL_CC_FILE \
	HL_INC_TEXT HL_INC_CMD HL_INC_PROJECT HL_INC_PROFILE HL_INC_FILE \
	HL_SLIB_MK_TEXT HL_SLIB_MK_CMD HL_SLIB_MK_PROJECT HL_SLIB_MK_PROFILE HL_SLIB_MK_FILE \
	HL_SLIB_TEXT HL_SLIB_CMD HL_SLIB_PROJECT HL_SLIB_PROFILE HL_SLIB_FILE \
	HL_DLIB_MK_TEXT HL_DLIB_MK_CMD HL_DLIB_MK_PROJECT HL_DLIB_MK_PROFILE HL_DLIB_MK_FILE \
	HL_DLIB_TEXT HL_DLIB_CMD HL_DLIB_PROJECT HL_DLIB_PROFILE HL_DLIB_FILE \
	HL_EXE_MK_TEXT HL_EXE_MK_CMD HL_EXE_MK_PROJECT HL_EXE_MK_PROFILE HL_EXE_MK_FILE \
	HL_EXE_TEXT HL_EXE_CMD HL_EXE_PROJECT HL_EXE_PROFILE HL_EXE_FILE \
	HL_CLEAR
HLNOHL_VARS := \
	HL_PRJ_MK_TEXT \
	HL_CC_TEXT \
	HL_INC_TEXT \
	HL_SLIB_MK_TEXT \
	HL_SLIB_TEXT \
	HL_DLIB_MK_TEXT \
	HL_DLIB_TEXT \
	HL_EXE_MK_TEXT \
	HL_EXE_TEXT

HL_CLEAR := [0m
HL_READY_TEXT := READY  $(empty)
HL_READY_CMD := [97m
HL_READY_PROJECT := [92m
HL_READY_PROFILE := [93m
HL_READY_FILE := [32m
HL_PRJ_MK_TEXT := PRJ_MK $(empty)
HL_PRJ_MK_CMD := [36m
HL_PRJ_MK_PROJECT := [36m
HL_PRJ_MK_PROFILE := [36m
HL_PRJ_MK_FILE := [36m
HL_CC_TEXT := CC     $(empty)
HL_CC_CMD := [36m
HL_CC_PROJECT := [36m
HL_CC_PROFILE := [36m
HL_CC_FILE := [36m
HL_INC_TEXT := INC    $(empty)
HL_INC_CMD := [36m
HL_INC_PROJECT := [36m
HL_INC_PROFILE := [36m
HL_INC_FILE := [36m
HL_SLIB_MK_TEXT := SLIB_MK$(empty)
HL_SLIB_MK_CMD := [36m
HL_SLIB_MK_PROJECT := [36m
HL_SLIB_MK_PROFILE := [36m
HL_SLIB_MK_FILE := [36m
HL_SLIB_TEXT := SLIB   $(empty)
HL_SLIB_CMD := [36m
HL_SLIB_PROJECT := [36m
HL_SLIB_PROFILE := [36m
HL_SLIB_FILE := [96m
HL_DLIB_MK_TEXT := DLIB_MK$(empty)
HL_DLIB_MK_CMD := [36m
HL_DLIB_MK_PROJECT := [36m
HL_DLIB_MK_PROFILE := [36m
HL_DLIB_MK_FILE := [36m
HL_DLIB_TEXT := DLIB   $(empty)
HL_DLIB_CMD := [36m
HL_DLIB_PROJECT := [36m
HL_DLIB_PROFILE := [36m
HL_DLIB_FILE := [96m
HL_EXE_MK_TEXT := EXE_MK $(empty)
HL_EXE_MK_CMD := [36m
HL_EXE_MK_PROJECT := [36m
HL_EXE_MK_PROFILE := [36m
HL_EXE_MK_FILE := [36m
HL_EXE_TEXT := EXE    $(empty)
HL_EXE_CMD := [36m
HL_EXE_PROJECT := [36m
HL_EXE_PROFILE := [36m
HL_EXE_FILE := [96m

# HL_MSG $1=project $2=profile $3=file $4=type
hl_msg = $(HL_$4_CMD)$(HL_$4_TEXT)$(HL_CLEAR) $(HL_$4_PROJECT)$(project_name_seg.$1)$(HL_CLEAR) $(HL_$4_PROFILE)$(profile_name_seg.$2)$(HL_CLEAR)    $(HL_$4_FILE)$3$(HL_CLEAR)
nohl_msg = $(HL_$4_TEXT) $(project_name_seg.$1) $(profile_name_seg.$2)    $3

ifeq ($(nohl),)
progress_msg = $(call hl_msg,$1,$2,$3,$4)
else
progress_msg = $(call nohl_msg,$1,$2,$3,$4)
endif


# detect workspace file {{{
ifeq ($(ws_file),)
ws_lookup_list := $(foreach d,$(WS_LOOKUP_DIR_LIST),$d$(user_name)-$(host_name)$(WS_FILE_SUFFIX) $d$(WS_FILE_DEFAULT_NAME))
ws_file := $(firstword $(wildcard $(ws_lookup_list)))
endif # end - detect workspace file }}}

ifeq ($(ws_file)$(no_ws),)
$(error no workspace detected)
endif

# set default workspace vars {{{
ws_file_dir := $(call unts_path,$(or $(dir $(ws_file)),.))
root := .
project_file_list := **$(PROJECT_FILE_SUFFIX)
temp_root := _temp
out_root := _out
project_out_dir = $(out_root)/$(profile)/$(project)
project_temp_dir = $(temp_root)/$(profile)/$(project)
# default profiles
profile_list :=
# end - set default workspace vars }}}

# default Unix-like profiles {{{
ifneq ($(host_is_unixlike),)

p := cc-static-debug
profile_list += $p
tags.$p := cc static debug

p := cc-static-release
profile_list += $p
tags.$p := cc static release strip

p := cc-dynamic-debug
profile_list += $p
tags.$p := cc dynamic debug

p := cc-dynamic-release
profile_list += $p
tags.$p := cc dynamic release strip

endif # }}}
# default Windows profiles {{{
ifneq ($(host_is_windows),)

p := msvc-static-debug
profile_list += $p
tags.$p := msvc static debug windows

p := msvc-static-release
profile_list += $p
tags.$p := msvc static release windows

p := msvc-dynamic-debug
profile_list += $p
tags.$p := msvc dynamic debug windows

p := msvc-dynamic-release
profile_list += $p
tags.$p := msvc dynamic release windows

endif # }}}

# include workspace file {{{
ifneq ($(ws_file),)
include $(ws_file)
endif #}}}

# calculate workspace vars {{{
ws_root := $(call unts_path,$(call combine_path,$(root),$(ws_file_dir)))
out_root := $(call unts_path,$(call combine_path,$(out_root),$(ws_root)))
temp_root := $(call unts_path,$(call combine_path,$(temp_root),$(ws_root)))
project_file_list := $(call expand_wildcard,$(call combine_path,$(project_file_list),$(ws_root)))
project_file_to_name = $(patsubst %$(PROJECT_FILE_SUFFIX),%,$(notdir $1))
project_list := $(call project_file_to_name,$(project_file_list))
$(foreach f,$(project_file_list),$(eval project_file.$(call project_file_to_name,$f) := $f))
#}}}


# compute profile vars: init with defaults, normalize {{{
define compute_profile_vars

$(eval project_list.$1 := $(or $(projects.$1),$(project_list)))
$(eval tags.$1 := $(tags.$1))
$(foreach t,$(tags.$1),$(eval tag.$t.$1 := y))
$(eval out_inc_sub_dir.$1 := $(call unts_path,$(or $(out_inc_sub_dir.$1),include)))
$(eval out_slib_sub_dir.$1 := $(call unts_path,$(or $(out_slib_sub_dir.$1),lib)))
$(eval out_dlib_sub_dir.$1 := $(call unts_path,$(or $(out_dlib_sub_dir.$1),$(if $(tag.windows.$1),bin,lib))))
$(eval out_exe_sub_dir.$1 := $(call unts_path,$(or $(out_exe_sub_dir.$1),bin)))

$(call init_var,path.$1,$(PATH))

$(call init_var,slib_prefix.$1,$(if $(tag.cc.$1),lib,))
$(call init_var,dlib_prefix.$1,$(if $(tag.windows.$1),,lib))
$(call init_var,exe_prefix.$1,$(if $(tag.cc.$1),,))

$(call init_var,slib_suffix.$1,$(if $(tag.cc.$1),.a)$(if $(tag.msvc.$1),.lib))
$(call init_var,dlib_suffix.$1,$(if $(tag.windows.$1),.dll,.so))
$(call init_var,exe_suffix.$1,$(if $(tag.windows.$1),.exe,))
$(call init_var,obj_suffix.$1,$(if $(tag.windows.$1),.obj,.o))

$(call init_var,opt_cpp_include.$1,$(if $(tag.msvc.$1),/I,-I))
$(call init_var,opt_cpp_define.$1,$(if $(tag.msvc.$1),/D,-D))
$(call init_var,opt_use_lib_file.$1,$(if $(tag.msvc.$1),,-l:))

$(call init_var,lib_dep_dirs.$1,$(call combine_path,$(lib_dep_dirs.$1),$(ws_root)))

$(if $(tag.cc.$1),$(call init_var,cc_prefix.$1,))
$(if $(tag.cc.$1),$(call init_var,cc_suffix.$1,))

$(if $(tag.gcc.$1),$(call init_var,cc_name.$1,gcc))
$(if $(tag.clang.$1),$(call init_var,cc_name.$1,clang))
$(if $(tag.cc.$1),$(call init_var,cc_name.$1,cc))

$(if $(tag.cc.$1),$(call init_var,cc_dep.$1,y))

$(if $(tag.gcc.$1),$(call init_var,cxx_name.$1,g++))
$(if $(tag.clang.$1),$(call init_var,cxx_name.$1,clang++))
$(if $(tag.cc.$1),$(call init_var,cxx_name.$1,c++))

$(if $(tag.cc.$1),$(call init_var,ar_name.$1,ar))
$(if $(tag.cc.$1),$(call init_var,ar_flags.$1,rcs))

$(if $(tag.cc.$1),$(call init_var,strip.$1,$(cc_prefix.$1)strip$(cc_suffix.$1)))
$(if $(tag.cc.$1),$(call init_var,ar.$1,$(cc_prefix.$1)$(ar_name.$1)$(cc_suffix.$1)))
$(if $(tag.cc.$1),$(call init_var,cc.$1,$(cc_prefix.$1)$(cc_name.$1)$(cc_suffix.$1)))
$(if $(tag.cc.$1),$(call init_var,cxx.$1,$(cc_prefix.$1)$(cxx_name.$1)$(cc_suffix.$1)))

$(call init_var,build_rule.slib.$1,gen_product.$(filter cc msvc,$(tags.$1)).slib)
$(call init_var,build_rule.dlib.$1,gen_product.$(filter cc msvc,$(tags.$1)).dlib)
$(call init_var,build_rule.exe.$1,gen_product.$(filter cc msvc,$(tags.$1)).exe)
$(call init_var,src_to_obj_rule.c.$1,gen_c_obj.$(filter cc msvc,$(tags.$1)))
$(call init_var,src_to_obj_rule.cxx.$1,gen_cxx_obj.$(filter cc msvc,$(tags.$1)))

$(if $(tag.msvc.$1),$(call init_var,vc_cl.$1,cl.exe))
$(if $(tag.msvc.$1),$(call init_var,vc_lib.$1,lib.exe))
$(if $(tag.msvc.$1),$(call init_var,vc_link.$1,link.exe))

$(if $(tag.msvc.$1),$(call init_var,vc_cl_flags.$1,/nologo /c))
$(if $(tag.msvc.$1),$(call init_var,vc_cl_out.$1,/Fo:))

$(if $(tag.msvc.$1),$(call init_var,vc_lib_flags.$1,/nologo))
$(if $(tag.msvc.$1),$(call init_var,vc_lib_out.$1,/out:))

$(if $(tag.msvc.$1),$(call init_var,vc_link_flags.$1,/nologo))
$(if $(tag.msvc.$1),$(call init_var,vc_link_out.$1,/out:))

$(if $(tag.msvc.$1),$(call init_var,msvc_dep.$1,y))

$(eval profile_name_seg.$1 := $(call ualnum_tpad,$1,39)$$(empty))

endef
$(foreach p,$(profile_list),$(call compute_profile_vars,$p))
# end - compute_profile_vars }}}

slib_export._default = $(call cp_file,$3,$4)
dlib_export._default = $(if $(and $(tag.cc.$2),$(tag.release.$2)),$$(cc_prefix.$2)strip$$(cc_suffix.$2) $3 -o $4,$(call cp_file,$3,$4))
exe_export._default = $(if $(and $(tag.cc.$2),$(tag.release.$2)),$$(cc_prefix.$1)strip$$(cc_suffix.$1) $3 -o $4,$(call cp_file,$3,$4))

# compute_profile_rules $1=profile {{{
define compute_profile_rules

$(foreach v,$(PROFILE_RULES),$(if $(filter undefined,$(origin $v.$1)),$(eval $v.$1 := $v._default)))

endef
$(foreach p,$(profile_list),$(call compute_profile_rules,$p))
# end - compute_profile_rules }}}

# prepare profile targets {{{
define prepare_profile_targets

.PHONY: $1 build-$1 clean-$1 $(tags.$1) info-$1
$1: build-$1
all: build-$1
$(foreach t,$(tags.$1),$(enter)$t: build-$1)
info-profiles: info-$1
info-$1:
	@echo $1
	@echo - project list: $(project_list.$1)
	$(foreach v,$(PROFILE_VARS),$(call rule_cmd,@echo - $v: $($v.$1)))

endef
$(foreach p,$(profile_list),$(eval $(call prepare_profile_targets,$p)))
# end prepare profile targets }}}

# if_project_in_profile $1=project $2=profile $3=on_true $4=on_false
if_project_in_profile = $(if $(filter $1,$(project_list.$2)),$3,$4)

# set_project_vars.slib $1=project $2=profile
define set_project_vars.slib
$(eval out_inc_dir.$2.$1 := $(call unts_path,$(call uats_path,$(out_dir.$2.$1))$(out_inc_sub_dir)))
$(eval out_slib_dir.$2.$1 := $(call unts_path,$(call uats_path,$(out_dir.$2.$1))$(out_slib_sub_dir)))
$(eval out_slib_path.$2.$1 := $(out_slib_dir.$2.$1)/$(slib_prefix.$2.$1)$(name.$2.$1)$(slib_suffix.$2.$1))
$(eval out_use_slib_path.$2.$1 := $(call uats_path,$(out_dir.$2.$1))$1$(USE_SLIB_SUFFIX))
$(eval temp_slib_path.$2.$1 := $(call temp_dir.$2.$1)/$(notdir $(out_slib_path.$2.$1)))
$(off_info slib_export.$2.$1 = $(call slib_export.$2.$1,SRC,DEST))
endef

# set_project_vars.dlib $1=project $2=profile
define set_project_vars.dlib
$(eval out_inc_dir.$2.$1 := $(call unts_path,$(call uats_path,$(out_dir.$2.$1))$(out_inc_sub_dir)))
$(eval out_dlib_dir.$2.$1 := $(call unts_path,$(call uats_path,$(out_dir.$2.$1))$(out_dlib_sub_dir)))
$(eval out_dlib_path.$2.$1 := $(out_dlib_dir.$2.$1)/$(dlib_prefix.$2.$1)$(name.$2.$1)$(dlib_suffix.$2.$1))
$(eval out_slib_dir.$2.$1 := $(call unts_path,$(call uats_path,$(out_dir.$2.$1))$(out_slib_sub_dir)))
$(eval out_slib_path.$2.$1 := $(out_slib_dir.$2.$1)/$(slib_prefix.$2.$1)$(name.$2.$1)$(slib_suffix.$2.$1))
$(eval out_use_dlib_path.$2.$1 := $(call uats_path,$(out_dir.$2.$1))$1$(USE_DLIB_SUFFIX))
$(eval temp_dlib_path.$2.$1 := $(call temp_dir.$2.$1)/$(notdir $(out_dlib_path.$2.$1)))
endef

# set_project_vars.exe $1=project $2=profile
define set_project_vars.exe
$(eval out_exe_dir.$2.$1 := $(call unts_path,$(call uats_path,$(out_dir.$2.$1))$(out_exe_sub_dir)))
$(eval out_exe_path.$2.$1 := $(out_exe_dir.$2.$1)/$(exe_prefix.$2.$1)$(name.$2.$1)$(exe_suffix.$2.$1))
$(eval temp_exe_path.$2.$1 := $(call temp_dir.$2.$1)/$(notdir $(out_exe_path.$2.$1)))
endef


# process_project_profile_1 $1=project $2=profile
define process_project_profile_1

$(eval profile := $2)
$(foreach v,$(PROFILE_VARS),$(eval $v.$2.$1 := $(or $($v.$2.$1),$($v.$1),$($v.$2))))
$(foreach v,$(PROFILE_VARS),$(eval $v := $($v.$2.$1)))
$(foreach t,$(tags),$(eval $t := y))
$(foreach v,$(PROFILE_RULES),$(eval $v.$2.$1 := $(or $($v.$2.$1),$($v),$($v.$2))))

$(foreach v,$(PROJECT_VARS),$(eval $v.$2.$1 := $($v)))

$(eval project := $1)
$(eval product.$2.$1 := $(patsubst lib,$(if $(static),slib,dlib),$(product)))
$(if $(product.$2.$1),$(if $(and $(filter 1,$(words $(product.$2.$1))),$(filter $(product.$2.$1),$(PRODUCTS))),$(eval profile_has_project.$2.$1 := y),$(error bad product "$(product.$2.$1)" for project $1 in profile $2)),$(off-info no product for project $1 in profile $2) $(eval project_list.$2 := $(filter-out $1,$(project_list.$2))))
$(eval product.$(product.$2.$1) := y)
$(eval name.$2.$1 := $(or $(name),$1))

$(eval lib_deps.$2.$1 := $(lib_deps))

$(eval project_deps.$2.$1 := $(foreach d,$(lib_deps.$2.$1),$(or $(resolve_dep.$2.$d),$(patsubst %,%-$2,$(filter $d,$(project_list.$2))))))
$(eval temp_dir.$2.$1 := $(call unts_path,$(or $(temp_dir.$2.$1),$(project_temp_dir),_temp)))
$(eval out_dir.$2.$1 := $(call unts_path,$(or $(out_dir.$2.$1),$(project_out_dir),_out)))

$(call set_project_vars.$(product.$2.$1),$1,$2)

$(eval want.debug_info.$2.$1 := y)

$(eval project_base_dir.$2.$1 := $(call unts_path,$(call combine_path,$(dir $(project_file.$1)),$(or $(project_base_dir),.))))

$(eval src_to_obj_types.$2.$1 := $(sort $(if $(c.sources),c) $(if $(cxx.sources),cxx) $(src_to_obj_types)))

$(foreach t,$(src_to_obj_types.$2.$1),$(eval $t.sources.$2.$1 := $(call expand_wildcard,$(call combine_path,$($t.sources),$(project_base_dir.$2.$1)))))

$(eval pub_inc_dir.$2.$1 := $(call combine_path,$(pub_inc_dir),$(project_base_dir.$2.$1)))
$(eval pub_inc_files.$2.$1 := $(call expand_wildcard,$(call combine_path,$(pub_inc_files),$(pub_inc_dir.$2.$1))))
$(foreach f,$(pub_inc_files.$2.$1),$(eval out_inc_file.$2.$1!$f := $(patsubst $(pub_inc_dir.$2.$1)/%,$(out_inc_dir.$2.$1)/%,$f)))
$(eval cpp_flags.$2.$1 := $(cpp_flags) $(patsubst %,$(opt_cpp_inc_dir.$2)%,$(cpp_inc_dirs)) $(patsubst %,$(opt_cpp_define.$2)%,$(cpp_defines)))
$(eval c_flags.$2.$1 := $(c_flags) $(call c_cxx_flags.rule.debug_info,$1,$2))
$(eval cxx_flags.$2.$1 := $(cxx_flags) $(call c_cxx_flags.rule.debug_info,$1,$2))
$(eval user_cpp_flags.$2.$1 = $(value user_cpp_flags) $(patsubst %,$$$$(opt_cpp_inc_dir)%,$(user_cpp_inc_dirs)) $(patsubst %,$$$$(opt_cpp_define)%,$(user_cpp_defines)))
$(eval ld_flags.$2.$1 := $(ld_flags) $(call ld_flags.rule.debug_info,$1,$2))
$(eval user_ld_libs.$2.$1 := $(user_ld_libs))
$(eval user_ld_flags.$2.$1 := $(user_ld_flags))

$(foreach t,$(src_to_obj_types.$2.$1),$(foreach s,$($t.sources.$2.$1),\
	$(foreach v,$(SRC_VARS.$t),$(eval $v.$2.$1!$s := $($v!$(patsubst $(project_base_dir.$2.$1)/%,%,$s)))) \
	$(foreach v,$(SRC_VARS_WITH_PROJECT_FILES),$(eval $v.$2.$1!$s := $(call expand_wildcard,$(call combine_path,$($v!$(patsubst $(project_base_dir.$2.$1)/%,%,$s)),$(project_base_dir.$2.$1))))) \
	))

$(foreach t,c cxx,$(foreach s,$($t.sources.$2.$1),$(eval cpp_flags.$2.$1!$s += $(patsubst %,$(opt_cpp_define.$2)%,$(cpp_defines.$2.$1!$s)))))

$(eval project_mk_path.$2.$1 := $(temp_dir.$2.$1)/project.mk)
$(eval old_project_mk.$2.$1 := $$(file <$(project_mk_path.$2.$1)))

$(eval project_name_seg.$1 := $(call ualnum_tpad,$1,23)$$(empty))

$(if $(filter $(quick.$2),$1),$(eval quick_pp_list += build-$1-$2))

$(eval undefine product.$(product.$2.$1))
$(foreach t,$(tags),$(eval undefine $t))
$(foreach v,$(PROFILE_VARS),$(eval undefine $v))


endef

# process_project_profile_2 $1=project $2=profile
define process_project_profile_2

$(foreach d,$(lib_deps.$2.$1),$(eval lib_dep_dirs.$2.$1 += $(out_dir.$(or $(resolve_dep.$2.$d),$2).$d)))
$(eval new_project_mk.$2.$1 := $$(call gen_project_mk,$1,$2))

$(eval $(call gen_project_profile_rules,$1,$2))

endef

# gen_project_profile_rules $1=project $2=profile
define gen_project_profile_rules

.PHONY: $1 build-$1 clean-$1 $1-$2 $2-$1 build-$1-$2 build-$2-$1 clean-$1-$2 clean-$2-$1 vars-$1-$2 vars-$2-$1

$1: build-$1
$1-$2: build-$1-$2
$2-$1: build-$2-$1
build-$1: build-$1-$2
build-$2: build-$1-$2
build-$2-$1: build-$1-$2
build-$1-$2: $(project_mk_path.$2.$1) $(patsubst %,build-%,$(project_deps.$2.$1))
	$(cmd_pfx)$$(MAKE) -f $$< $1-$2
	$(call echo,$(call progress_msg,$1,$2,,READY))

clean-$1: clean-$1-$2
clean-$2: clean-$1-$2
clean-$2-$1: clean-$1-$2
clean-$1-$2:
	@echo cleaning profile=$1 project=$2
	-$(call rm_tree,$(temp_dir.$2.$1) $(out_dir.$2.$1))

vars-$1: vars-$1-$2
vars-$2-$1: vars-$1-$2
vars-$1-$2:
	@echo $1-$2:
	$(foreach v,$(PROJECT_VARS),$(call rule_cmd,@echo - $v: $($v.$2.$1)))
	$(foreach v,$(PROFILE_VARS),$(call rule_cmd,@echo - $v: $($v.$2.$1)))

$$(if $$(and $$(findstring $$(new_project_mk.$2.$1),$$(old_project_mk.$2.$1)),$$(findstring $$(old_project_mk.$2.$1),$$(new_project_mk.$2.$1)$$(enter))),,.PHONY: $(project_mk_path.$2.$1))


$(project_mk_path.$2.$1): $(invoked_makefile) $(project_file.$1) $(ws_file) | $(temp_dir.$2.$1)
	$(call echo,$(call progress_msg,$1,$2,$(project_mk_path.$2.$1),PRJ_MK))
	$$(file > $(project_mk_path.$2.$1),$$(new_project_mk.$2.$1))

$(temp_dir.$2.$1):
	$$(cmd_pfx)$(call make_dir,$(temp_dir.$2.$1))

endef


# src_to_temp_path $1=project $2=profile $3=src $4=suffix
src_to_temp_path = $(patsubst %,$(temp_dir.$2.$1)/%$4,$(subst /,!,$3))

# gen_c_obj.cc $1=project $2=profile $3=src
define gen_c_obj.cc

-include $(call src_to_temp_path,$1,$2,$3,.d)
$(call src_to_temp_path,$1,$2,$3,$(obj_suffix.$2.$1)): $3 $(call src_to_temp_path,$1,$2,$3,.d) $(dep.$2.$1!$3) $$(project_mk) | $(temp_dir.$2.$1)
	$(call echo,$$(call progress_msg,1,2,$3,CC))
	$$(cmd_pfx)$(cc.$2) -c -o$$@ $(c_flags_pre.$2) $(cpp_flags.$2.$1!$3) $$(cpp_flags) $(c_flags.$2.$1) $(c_flags.$2.$1!$3) $(if $(cc_dep.$2.$1),-MT $$@ -MMD -MF $(call src_to_temp_path,$1,$2,$3,.d)) $(c_flags_post.$2) $$<

$(call src_to_temp_path,$1,$2,$3,.d): ;


endef

# gen_cxx_obj.cc $1=project $2=profile $3=src
define gen_cxx_obj.cc

-include $(call src_to_temp_path,$1,$2,$3,.d)
$(call src_to_temp_path,$1,$2,$3,$(obj_suffix.$2.$1)): $3 $(call src_to_temp_path,$1,$2,$3,.d) $(dep.$2.$1!$3) $$(project_mk) | $(temp_dir.$2.$1)
	$(call echo,$$(call progress_msg,1,2,$3,CC))
	$$(cmd_pfx)$(cxx.$2) -c -o$$@ $(cxx_flags_pre.$2) $(cpp_flags.$2.$1!$3) $$(cpp_flags) $(cxx_flags.$2.$1) $(cxx_flags.$2.$1!$3) $(if $(cc_dep.$2.$1),-MT $$@ -MMD -MF $(call src_to_temp_path,$1,$2,$3,.d)) $(cxx_flags_post.$2) $$<

$(call src_to_temp_path,$1,$2,$3,.d): ;


endef


define make_dir_rule

$1:
	$$(cmd_pfx)$(call make_dir,$1)

endef

make_dir_rules = $(foreach d,$1,$(call make_dir_rule,$(call unts_path,$d)))

# export_inc_file $1=project $2=profile $3=pub_inc_file
define export_inc_file_inner
$1-$2: $4
$4: $3 | $(call unts_path,$(dir $4))
	$(call echo,$$(call progress_msg,1,2,$3,INC))
	$$(cmd_pfx)$(call cp_file,$3,$4)

endef

# export_inc_file $1=project $2=profile $3=pub_inc_file
export_inc_file = $(call export_inc_file_inner,$1,$2,$3,$(out_inc_file.$2.$1!$3))

# $1=project $2=profile
src_list = $(foreach t,$(src_to_obj_types.$2.$1),$($t.sources.$2.$1))
# $1=project $2=profile
obj_list = $(call src_to_temp_path,$1,$2,$(call src_list,$1,$2),$(obj_suffix.$2.$1))

# gen_product.x.y $1=project $2=profile
define gen_product.cc.slib

export CPATH=$$(subst $$(space),:,$$(foreach p,$$(cpp_includes) $$(c_includes),$$p))

$1-$2: $1-$2-slib

.DELETE_ON_ERROR:

$1-$2-slib: $(out_slib_path.$2.$1) $(out_use_slib_path.$2.$1)

define slib_mk_content
slib_mk_dir.$1 := $$$$(dir $$$$(lastword $$$$(MAKEFILE_LIST)))
cpp_includes += $$$$(slib_mk_dir.$1)$(out_inc_sub_dir.$2.$1)
lib_includes += $$$$(slib_mk_dir.$1)$(out_slib_sub_dir.$2.$1)
cpp_flags += $(user_cpp_flags.$2.$1)
ld_flags += $(user_ld_flags.$2.$1)
ld_libs += $$$$(opt_use_lib_file)$(notdir $(out_slib_path.$2.$1)) $(patsubst %,$$$$(opt_use_lib_file)%,$(user_ld_libs.$2.$1))
product_dep += $$$$(slib_mk_dir.$1)$(out_slib_sub_dir.$2.$1)/$(notdir $(out_slib_path.$2.$1))
endef

$(out_use_slib_path.$2.$1): $$(project_mk) | $(call unts_path,$(dir $(out_use_slib_path.$2.$1)))
	$(call echo,$$(call progress_msg,1,2,$$@,SLIB_MK))
	$$(file >$(out_use_slib_path.$2.$1),$$(slib_mk_content))

$(out_slib_path.$2.$1): $(temp_slib_path.$2.$1) $$(project_mk) | $(call unts_path,$(dir $(out_slib_path.$2.$1)))
	$$(cmd_pfx)$(call $(slib_export.$2.$1),$1,$2,$(temp_slib_path.$2.$1),$(out_slib_path.$2.$1))

$(temp_slib_path.$2.$1): $(call obj_list,$1,$2) | $(call unts_path,$(dir $(temp_slib_path.$2.$1)))
	$(call echo,$$(call progress_msg,1,2,$(out_slib_path.$2.$1),SLIB))
	$$(cmd_pfx)$(ar.$2) $(ar_flags.$2) $$@ $$^

$(foreach h,$(pub_inc_files.$2.$1),$(call export_inc_file,$1,$2,$h))

$(call $(custom_rules.$2.$1),$1,$2)

endef


# gen_product.x.y $1=project $2=profile
define gen_product.cc.dlib

export CPATH=$$(subst $$(space),:,$$(foreach p,$$(cpp_includes) $$(c_includes),$$p))
#export LIBRARY_PATH=$$(subst $$(space),:,$$(foreach p,$$(lib_includes),$$p))
ld_flags += $$(foreach p,$$(lib_includes),-L$$p)

$1-$2: $1-$2-dlib

.DELETE_ON_ERROR:

$1-$2-dlib: $(out_dlib_path.$2.$1) $(out_use_dlib_path.$2.$1)

define dlib_mk_content
dlib_mk_dir.$1 := $$$$(dir $$$$(lastword $$$$(MAKEFILE_LIST)))
cpp_includes += $$$$(dlib_mk_dir.$1)$(out_inc_sub_dir.$2.$1)
lib_includes += $$$$(dlib_mk_dir.$1)$(out_dlib_sub_dir.$2.$1)
cpp_flags += $(user_cpp_flags.$2.$1)
ld_flags += $(user_ld_flags.$2.$1)
ld_libs += $$$$(opt_use_lib_file)$(notdir $(out_dlib_path.$2.$1)) $(patsubst %,$$$$(opt_use_lib_file)%,$(user_ld_libs.$2.$1))
product_dep += $$$$(dlib_mk_dir.$1)$(out_dlib_sub_dir.$2.$1)/$(notdir $(out_dlib_path.$2.$1))
endef

$(out_use_dlib_path.$2.$1): $$(project_mk) | $(call unts_path,$(dir $(out_use_dlib_path.$2.$1)))
	$(call echo,$$(call progress_msg,1,2,$$@,DLIB_MK))
	$$(file >$(out_use_dlib_path.$2.$1),$$(dlib_mk_content))

$(out_dlib_path.$2.$1): $(temp_dlib_path.$2.$1) $$(project_mk) | $(call unts_path,$(dir $(out_dlib_path.$2.$1)))
	$$(cmd_pfx)$(call $(dlib_export.$2.$1),$1,$2,$(temp_dlib_path.$2.$1),$(out_dlib_path.$2.$1))

$(temp_dlib_path.$2.$1): $(call obj_list,$1,$2) $$(product_dep) | $(call unts_path,$(dir $(temp_dlib_path.$2.$1)))
	$(call echo,$$(call progress_msg,1,2,$(out_dlib_path.$2.$1),DLIB))
	$$(cmd_pfx)$(if $(cxx.sources.$2.$1),$(cxx.$2),$(cc.$2)) -shared -o $$@ $(ld_flags_pre.$2) $$(ld_flags) $$(ld_flags_post.$2) $(call obj_list,$1,$2) $$(ld_libs) $$(ld_sys_libs)

$(foreach h,$(pub_inc_files.$2.$1),$(call export_inc_file,$1,$2,$h))

$(call $(custom_rules.$2.$1),$1,$2)

endef

# gen_product.x.y $1=project $2=profile
define gen_product.cc.exe

export CPATH=$$(subst $$(space),:,$$(foreach p,$$(cpp_includes) $$(c_includes),$$p))
#export LIBRARY_PATH=$$(subst $$(space),:,$$(foreach p,$$(lib_includes),$$p))
ld_flags += $$(foreach p,$$(lib_includes),-L$$p)

$1-$2: $1-$2-exe

.DELETE_ON_ERROR:

$1-$2-exe: $(out_exe_path.$2.$1)

define exe_mk_content
exe_mk_dir.$1 := $$$$(dir $$$$(lastword $$$$(MAKEFILE_LIST)))
exe_includes += $$$$(exe_mk_dir.$1)$(out_exe_sub_dir.$2.$1)
endef

$(out_exe_path.$2.$1): $(temp_exe_path.$2.$1) $$(project_mk) | $(call unts_path,$(dir $(out_exe_path.$2.$1)))
	$$(cmd_pfx)$(call $(exe_export.$2.$1),$1,$2,$(temp_exe_path.$2.$1),$(out_exe_path.$2.$1))

$(temp_exe_path.$2.$1): $(call obj_list,$1,$2) $$(product_dep) | $(call unts_path,$(dir $(temp_exe_path.$2.$1)))
	$(call echo,$$(call progress_msg,1,2,$(out_exe_path.$2.$1),EXE))
	$$(cmd_pfx)$(if $(cxx.sources.$2.$1),$(cxx.$2),$(cc.$2)) -o $$@ $(ld_flags_pre.$2) $$(ld_flags) $$(ld_flags_post.$2) $(call obj_list,$1,$2) $$(ld_libs) $$(ld_sys_libs)

$(foreach h,$(pub_inc_files.$2.$1),$(call export_inc_file,$1,$2,$h))

$(call $(custom_rules.$2.$1),$1,$2)

endef


# gen_c_obj.msvc $1=project $2=profile $3=src
define gen_c_obj.msvc

-include $(call src_to_temp_path,$1,$2,$3,.d)
$(call src_to_temp_path,$1,$2,$3,$(obj_suffix.$2.$1)): $3 $(call src_to_temp_path,$1,$2,$3,.d) $(dep.$2.$1!$3) $$(project_mk) | $(temp_dir.$2.$1)
	$(call echo,$$(call progress_msg,1,2,$3,CC))
	$$(cmd_pfx)$(vc_cl.$2) $(vc_cl_flags.$2) $(vc_cl_out.$2)$$@ $(c_flags_pre.$2) $(cpp_flags.$2.$1!$3) $$(cpp_flags) $(c_flags.$2.$1) $(c_flags.$2.$1!$3) $(if $(msvc_dep.$2.$1),/showIncludes) $(c_flags_post.$2) $$< > $(call src_to_temp_path,$1,$2,$3,.clout) && echo Note: ok >> $(call src_to_temp_path,$1,$2,$3,.clout) || echo Note: failed >> $(call src_to_temp_path,$1,$2,$3,.clout)
	-@grep "): \(warning\|error\)" $(call src_to_temp_path,$1,$2,$3,.clout)
	@grep -q "^Note: ok" $(call src_to_temp_path,$1,$2,$3,.clout)
	$$(cmd_pfx)$(if $(msvc_dep.$2.$1),echo $(call src_to_temp_path,$1,$2,$3,$(obj_suffix.$2.$1)):\> $(call src_to_temp_path,$1,$2,$3,.d))
	$$(cmd_pfx)$(if $(msvc_dep.$2.$1),grep "^Note: including file: " $(call src_to_temp_path,$1,$2,$3,.clout) | sed "s@\\@/@g" | sed "s/^Note: including file: *\(.*\)/\1/" | sed "s/ /\\ /g" | sed "s/\(.*\)/\1 \\/" >> $(call src_to_temp_path,$1,$2,$3,.d))
	$$(cmd_pfx)$(if $(msvc_dep.$2.$1),echo $(call src_to_temp_path,$1,$2,$3,.d) >> $(call src_to_temp_path,$1,$2,$3,.d))
	$$(cmd_pfx)$(if $(msvc_dep.$2.$1),touch -c $(call src_to_temp_path,$1,$2,$3,$(obj_suffix.$2.$1)))

$(call src_to_temp_path,$1,$2,$3,.d): ;

endef

# gen_cxx_obj.msvc $1=project $2=profile $3=src
define gen_cxx_obj.msvc

-include $(call src_to_temp_path,$1,$2,$3,.d)
$(call src_to_temp_path,$1,$2,$3,$(obj_suffix.$2.$1)): $3 $(call src_to_temp_path,$1,$2,$3,.d) $(dep.$2.$1!$3) $$(project_mk) | $(temp_dir.$2.$1)
	$(call echo,$$(call progress_msg,1,2,$3,CC))
	$$(cmd_pfx)$(vc_cl.$2) $(vc_cl_flags.$2) $(vc_cl_out.$2)$$@ $(cxx_flags_pre.$2) $(cpp_flags.$2.$1!$3) $$(cpp_flags) $(cxx_flags.$2.$1) $(cxx_flags.$2.$1!$3) $(if $(msvc_dep.$2.$1),/showIncludes) $(cxx_flags_post.$2) $$< > $(call src_to_temp_path,$1,$2,$3,.clout) && echo Note: ok >> $(call src_to_temp_path,$1,$2,$3,.clout) || echo Note: failed >> $(call src_to_temp_path,$1,$2,$3,.clout)
	-@grep "): \(warning\|error\)" $(call src_to_temp_path,$1,$2,$3,.clout)
	@grep -q "^Note: ok" $(call src_to_temp_path,$1,$2,$3,.clout)
	$$(cmd_pfx)$(if $(msvc_dep.$2.$1),echo $(call src_to_temp_path,$1,$2,$3,$(obj_suffix.$2.$1)):\> $(call src_to_temp_path,$1,$2,$3,.d))
	$$(cmd_pfx)$(if $(msvc_dep.$2.$1),grep "^Note: including file: " $(call src_to_temp_path,$1,$2,$3,.clout) | sed "s@\\@/@g" | sed "s/^Note: including file: *\(.*\)/\1/" | sed "s/ /\\ /g" | sed "s/\(.*\)/\1 \\/" >> $(call src_to_temp_path,$1,$2,$3,.d))
	$$(cmd_pfx)$(if $(msvc_dep.$2.$1),echo $(call src_to_temp_path,$1,$2,$3,.d) >> $(call src_to_temp_path,$1,$2,$3,.d))
	$$(cmd_pfx)$(if $(msvc_dep.$2.$1),touch -c $(call src_to_temp_path,$1,$2,$3,$(obj_suffix.$2.$1)))

$(call src_to_temp_path,$1,$2,$3,.d): ;


endef

# gen_product.x.y $1=project $2=profile
define gen_product.msvc.slib

export INCLUDE:=$$(if $$(INCLUDE),$$(INCLUDE);)$$(subst $$(space),;,$$(foreach p,$$(cpp_includes) $$(c_includes),$$p))

$1-$2: $1-$2-slib

.DELETE_ON_ERROR:

$1-$2-slib: $(out_slib_path.$2.$1) $(out_use_slib_path.$2.$1)

define slib_mk_content
slib_mk_dir.$1 := $$$$(dir $$$$(lastword $$$$(MAKEFILE_LIST)))
cpp_includes += $$$$(slib_mk_dir.$1)$(out_inc_sub_dir.$2.$1)
lib_includes += $$$$(slib_mk_dir.$1)$(out_slib_sub_dir.$2.$1)
cpp_flags += $(user_cpp_flags.$2.$1)
ld_flags += $(user_ld_flags.$2.$1)
ld_libs += $$$$(opt_use_lib_file)$(notdir $(out_slib_path.$2.$1)) $(patsubst %,$$$$(opt_use_lib_file)%,$(user_ld_libs.$2.$1))
product_dep += $$$$(slib_mk_dir.$1)$(out_slib_sub_dir.$2.$1)/$(notdir $(out_slib_path.$2.$1))
endef

$(out_use_slib_path.$2.$1): $$(project_mk) | $(call unts_path,$(dir $(out_use_slib_path.$2.$1)))
	$(call echo,$$(call progress_msg,1,2,$$@,SLIB_MK))
	$$(file >$(out_use_slib_path.$2.$1),$$(slib_mk_content))

$(out_slib_path.$2.$1): $(temp_slib_path.$2.$1) $$(project_mk) | $(call unts_path,$(dir $(out_slib_path.$2.$1)))
	$$(cmd_pfx)$(call $(slib_export.$2.$1),$1,$2,$(temp_slib_path.$2.$1),$(out_slib_path.$2.$1))

$(temp_slib_path.$2.$1): $(call obj_list,$1,$2) | $(call unts_path,$(dir $(temp_slib_path.$2.$1)))
	$(call echo,$$(call progress_msg,1,2,$(out_slib_path.$2.$1),SLIB))
	$$(cmd_pfx)$(vc_lib.$2) $(vc_lib_flags.$2) $(vc_lib_out.$2)$$@ $$^

$(foreach h,$(pub_inc_files.$2.$1),$(call export_inc_file,$1,$2,$h))

$(call $(custom_rules.$2.$1),$1,$2)

endef


# gen_product.x.y $1=project $2=profile
define gen_product.msvc.dlib

export INCLUDE:=$$(if $$(INCLUDE),$$(INCLUDE);)$$(subst $$(space),;,$$(foreach p,$$(cpp_includes) $$(c_includes),$$p))
export LIB:=$$(if $$(LIB),$$(LIB);)$$(subst $$(space),;,$$(foreach p,$$(lib_includes),$$p))

$1-$2: $1-$2-dlib

.DELETE_ON_ERROR:

$1-$2-dlib: $(out_dlib_path.$2.$1) $(if $(want.debug_info.$2.$1),$(out_dlib_path.$2.$1:$(dlib_suffix.$2.$1)=.pdb)) $(out_slib_path.$2.$1) $(out_use_dlib_path.$2.$1)

define dlib_mk_content
dlib_mk_dir.$1 := $$$$(dir $$$$(lastword $$$$(MAKEFILE_LIST)))
cpp_includes += $$$$(dlib_mk_dir.$1)$(out_inc_sub_dir.$2.$1)
lib_includes += $$$$(dlib_mk_dir.$1)$(out_slib_sub_dir.$2.$1)
cpp_flags += $(user_cpp_flags.$2.$1)
ld_flags += $(user_ld_flags.$2.$1)
ld_libs += $$$$(opt_use_lib_file)$(notdir $(out_slib_path.$2.$1)) $(patsubst %,$$$$(opt_use_lib_file)%,$(user_ld_libs.$2.$1))
product_dep += $$$$(dlib_mk_dir.$1)$(out_slib_sub_dir.$2.$1)/$(notdir $(out_slib_path.$2.$1))
endef

$(out_use_dlib_path.$2.$1): $$(project_mk) | $(call unts_path,$(dir $(out_use_dlib_path.$2.$1)))
	$(call echo,$$(call progress_msg,1,2,$$@,DLIB_MK))
	$$(file >$(out_use_dlib_path.$2.$1),$$(dlib_mk_content))

$(out_dlib_path.$2.$1): $(temp_dlib_path.$2.$1) $$(project_mk) | $(call unts_path,$(dir $(out_dlib_path.$2.$1)))
	$$(cmd_pfx)$(call $(dlib_export.$2.$1),$1,$2,$(temp_dlib_path.$2.$1),$(out_dlib_path.$2.$1))

$(out_slib_path.$2.$1): $(temp_dlib_path.$2.$1) $$(project_mk) | $(call unts_path,$(dir $(out_slib_path.$2.$1)))
	$$(cmd_pfx)$(call cp_file,$(temp_dlib_path.$2.$1:$(dlib_suffix.$2.$1)=$(slib_suffix.$2.$1)),$(out_slib_path.$2.$1))

$(out_dlib_path.$2.$1:$(dlib_suffix.$2.$1)=.pdb): $(temp_dlib_path.$2.$1)
	$$(cmd_pfx)$(call cp_file,$(temp_dlib_path.$2.$1:$(dlib_suffix.$2.$1)=.pdb),$(out_dlib_path.$2.$1:$(dlib_suffix.$2.$1)=.pdb))

$(temp_dlib_path.$2.$1): $(call obj_list,$1,$2) $$(product_dep) | $(call unts_path,$(dir $(temp_dlib_path.$2.$1)))
	$(call echo,$$(call progress_msg,1,2,$(out_dlib_path.$2.$1),DLIB))
	$$(cmd_pfx)$(vc_link.$2) $(vc_link_flags.$2) /dll $(vc_link_out.$2)$$@ $(ld_flags_pre.$2) $$(ld_flags) $(ld_flags_post.$2) $(call obj_list,$1,$2) $$(ld_libs) $$(ld_sys_libs)

$(foreach h,$(pub_inc_files.$2.$1),$(call export_inc_file,$1,$2,$h))

$(call $(custom_rules.$2.$1),$1,$2)

endef

# gen_product.x.y $1=project $2=profile
define gen_product.msvc.exe

export INCLUDE:=$$(if $$(INCLUDE),$$(INCLUDE);)$$(subst $$(space),;,$$(foreach p,$$(cpp_includes) $$(c_includes),$$p))
export LIB:=$$(if $$(LIB),$$(patsubst %;,%,$$(LIB));)$$(subst $$(space),;,$$(foreach p,$$(lib_includes),$$p))

$1-$2: $1-$2-exe

.DELETE_ON_ERROR:

$1-$2-exe: $(out_exe_path.$2.$1) $(if $(want.debug_info.$2.$1),$(out_exe_path.$2.$1:$(exe_suffix.$2.$1)=.pdb))

define exe_mk_content
exe_mk_dir.$1 := $$$$(dir $$$$(lastword $$$$(MAKEFILE_LIST)))
exe_includes += $$$$(exe_mk_dir.$1)$(out_exe_sub_dir.$2.$1)
endef

$(out_exe_path.$2.$1): $(temp_exe_path.$2.$1) $$(project_mk) | $(call unts_path,$(dir $(out_exe_path.$2.$1)))
	$$(cmd_pfx)$(call $(exe_export.$2.$1),$1,$2,$(temp_exe_path.$2.$1),$(out_exe_path.$2.$1))

$(out_exe_path.$2.$1:$(exe_suffix.$2.$1)=.pdb): $(temp_exe_path.$2.$1)
	$$(cmd_pfx)$(call cp_file,$(temp_exe_path.$2.$1:$(exe_suffix.$2.$1)=.pdb),$(out_exe_path.$2.$1:$(exe_suffix.$2.$1)=.pdb))

$(temp_exe_path.$2.$1): $(call obj_list,$1,$2) $$(product_dep) | $(call unts_path,$(dir $(temp_exe_path.$2.$1)))
	$(call echo,$$(call progress_msg,1,2,$(out_exe_path.$2.$1),EXE))
	$$(cmd_pfx)$(vc_link.$2) $(vc_link_flags.$2) $(vc_link_out.$2)$$@ $(ld_flags_pre.$2) $$(ld_flags) $(ld_flags_post.$2) $(call obj_list,$1,$2) $$(ld_libs) $$(ld_sys_libs)

$(foreach h,$(pub_inc_files.$2.$1),$(call export_inc_file,$1,$2,$h))

$(call $(custom_rules.$2.$1),$1,$2)

endef


# gen_project_mk $1=project $2=profile
define gen_project_mk
project_mk := $$(lastword $$(MAKEFILE_LIST))
project := $1
profile := $2

cmd_pfx := @
empty :=
space := $$(empty) $$(empty)

$(subst $(enter)$(enter),,$(subst $(space)$(enter),$(enter),$(enter)$(foreach v,$(PROFILE_VARS) $(PROJECT_VARS) $(PROJECT_VARS.$(product.$2.$1)),$(enter)$v := $($v.$2.$1))$(foreach t,$(src_to_obj_types.$2.$1),$(foreach s,$($t.sources.$2.$1),$(foreach v,$(SRC_VARS_WITH_PROJECT_FILES) $(SRC_VARS.$t),$(enter)$v.$2.$1!$s := $($v.$2.$1!$s)$$(empty))))))


ifeq ($$(nohl),)
$(subst $(enter)$(enter),,$(subst $(space)$(enter),$(enter),$(enter)$(foreach v,$(HL_VARS),$(enter)$v := $($v)$$(empty))))
endif
$(subst $(enter)$(enter),,$(subst $(space)$(enter),$(enter),$(enter)$(foreach v,$(HLNOHL_VARS),$(enter)$v := $($v)$$(empty))))


progress_msg = $$(HL_$$4_CMD)$$(HL_$$4_TEXT)$$(HL_CLEAR) $$(HL_$$4_PROJECT)$(project_name_seg.$1)$$(HL_CLEAR) $$(HL_$$4_PROFILE)$(profile_name_seg.$2)$$(HL_CLEAR)    $$(HL_$$4_FILE)$$3$$(HL_CLEAR)

export PATH=$(path.$2)

.PHONY: $1-$2
$1-$2:

# expands words with "*" wildcard and keeps the words without "*"
expand_wildcard = $$(sort $$(foreach f,$$1,$$(if $$(findstring *,$$f),,$$f)) $$(wildcard $$(foreach f,$$1,$$(if $$(findstring **,$$f),$$(foreach m,* */* */*/* */*/*/* */*/*/*/*,$$(subst **,$$m,$$f)),$$(and $$(findstring *,$$f),$$f)))))

slib_dep_files := $$(call expand_wildcard,$$(patsubst %,%/**$(USE_SLIB_SUFFIX),$$(lib_dep_dirs)))
dlib_dep_files := $$(call expand_wildcard,$$(patsubst %,%/**$(USE_DLIB_SUFFIX),$$(lib_dep_dirs)))

dep_files := $(if $(tag.dynamic.$2),$$(dlib_dep_files) $$(slib_dep_files),$$(slib_dep_files) $$(dlib_dep_files))
$$(foreach d,$$(lib_deps),$$(eval include $$(firstword $$(filter %/$$d$(USE_SLIB_SUFFIX) %/$$d$(USE_DLIB_SUFFIX),$$(dep_files)))))

$(call $(build_rule.$(product.$2.$1).$2),$1,$2)
$(foreach t,$(src_to_obj_types.$2.$1),$(foreach s,$($t.sources.$2.$1),$(call $(src_to_obj_rule.$t.$2),$1,$2,$s)))

$(call make_dir_rules,$(sort $(temp_dir.$2.$1) $(out_dir.$2.$1) $(out_slib_dir.$2.$1) $(out_dlib_dir.$2.$1) $(out_exe_dir.$2.$1) $(dir $(patsubst $(pub_inc_dir.$2.$1)/%,$(out_inc_dir.$2.$1)/%,$(pub_inc_files.$2.$1)))))


dummy:;
endef

quick_pp_list :=

# process_project_1 $1=project
define process_project_1
$(foreach v,$(PROJECT_VARS) $(PROFILE_RULES),$(eval undefine $v))
$(eval include $(project_file.$1))

$(foreach v,$(PROFILE_VARS),$(if $(filter-out undefined,$(origin $v)),$(eval $v.$1 := $($v))))

$(foreach p,$(profile_list),$(call if_project_in_profile,$1,$p,$(call process_project_profile_1,$1,$p)))
endef
$(foreach q,$(project_list),$(call process_project_1,$q))

# process_project_2 $1=project
define process_project_2

$(eval .PHONY: $1 build-$1 clean-$1 vars-$1)
$(foreach p,$(profile_list),$(if $(profile_has_project.$p.$1),$(call process_project_profile_2,$1,$p)))

endef
$(foreach q,$(project_list),$(call process_project_2,$q))

.PHONY: all tare mare clean info info-profiles

#quick_profile_list := $(or $(sort $(foreach p,$(profile_list),$(if $(filter quick,$(tags.$p)),$p))),$(firstword $(profile_list)))
quick_profile_list := $(sort $(foreach p,$(profile_list),$(if $(filter quick,$(tags.$p)),$p)))
quick: $(patsubst %,build-%,$(quick_profile_list)) $(quick_pp_list)

tare: all
mare: all

info:
	@echo workspace file: $(ws_file)
	@echo workspace root: $(ws_root)
	@echo out root: $(out_root)
	@echo temp root: $(temp_root)
	@echo profiles:
	$(foreach p,$(profile_list),$(call rule_cmd,@echo - $p))
	@echo projects:
	$(foreach q,$(project_list),$(call rule_cmd,@echo - $q))

clean:
	-$(call rm_tree,$(temp_root))
	-$(call rm_tree,$(out_root))



endif # workspace

ifeq ($(baga_mode),vs)

vs_shared_path := $(wordlist 4,99,$(shell reg query HKLM\Software\Microsoft\VisualStudio\Setup /v SharedInstallationPath))
$(if $(vs_shared_path),$(eval vs_2017_root := $(shell "$(vs_shared_path)\..\Installer\vswhere.exe" -version [15.0,15.99] -property installationPath)))
$(if $(vs_shared_path),$(eval vs_2019_root := $(shell "$(vs_shared_path)\..\Installer\vswhere.exe" -version [16.0,16.99] -property installationPath)))

$(if $(VS110COMNTOOLS),$(eval vs_2012_root := $(call hnts_path,$(dir $(dir $(call unts_path,$(subst $(space),\\ ,$(VS110COMNTOOLS))))))))
$(if $(VS120COMNTOOLS),$(eval vs_2013_root := $(call hnts_path,$(dir $(dir $(call unts_path,$(subst $(space),\\ ,$(VS120COMNTOOLS))))))))
$(if $(VS140COMNTOOLS),$(eval vs_2015_root := $(call hnts_path,$(dir $(dir $(call unts_path,$(subst $(space),\\ ,$(VS140COMNTOOLS))))))))

x:
	@echo VS shared installation path: $(vs_shared_path)
	@echo vs_2012_root: $(vs_2012_root)
	@echo vs_2013_root: $(vs_2013_root)
	@echo vs_2015_root: $(vs_2015_root)
	@echo vs_2017_root: $(vs_2017_root)
	@echo vs_2019_root: $(vs_2019_root)

endif

