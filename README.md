# baga

Build Assistant for the General Audience

## How to use
make -f <path-to-baga.mk> OPTIONS

## Testing

On the host platform run:

    make tests

On Windows from a unix host:
- in Windows:
    - install OpenSSH server from Windows Features
    - create dir c:\work
    - add a batch named 'wdev64.cmd' somewhere in path to call Visual Studio's: vcvarsall amd64
- in host:
    - install key for passwordless ssh:
        ssh-copy-id user@windowsbox
    - create shell script wdev:
        ssh user@windowsbox "$@"
    - create shell script wdevcp:
        scp user@windowsbox
    - now run
        make wtest
      or simply
        make
