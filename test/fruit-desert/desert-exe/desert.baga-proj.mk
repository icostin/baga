product := exe
lib_deps := fruit
c.sources = **.c
c_flags = $(c_flags.lto_on_release)
ld_flags = $(if $(msvc),/subsystem:console) $(ld_flags.lto_on_release)
