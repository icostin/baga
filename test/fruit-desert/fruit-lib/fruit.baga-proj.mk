# lib is replaced with slib if build.static is set or dlib otherwise
product = lib

# c_sources items are relative to the location of .baga-proj.mk file
c.sources = **.c

# pub_inc_dir relative to 
pub_inc_dir = .
pub_inc_files = **.h

# cpp_inc_dirs items are prepended with -I or /I and put into CPPFLAGS
# and are injected in command lines for compiling c and c++ sources
cpp_inc_dirs =

# cpp_defines items are prepended with -D or /D and put into CPPFLAGS
# and are injected in command lines for compiling c and c++ sources
cpp_defines = FRUIT_LIB_BUILD $(if $(product.slib),FRUIT_LIB_STATIC)

cpp_defines!fruit.c = FOO BAR=2
cpp_flags!fruit.c = -DBLA
c_flags = \
	$(c_flags.config_optimizations) \
	$(c_flags.all_warnings_as_errors) \
	$(c_flags.lto_on_release)

dep!fruit.c = fruit.rebuild-tag
c_flags!fruit.c = $(if $(cc),-Wpedantic)

ld_flags = $(ld_flags.lto_on_release)

# user_cpp_defines are added as cpp_defines when building a component
# that declares it depends on this library
user_cpp_defines = $(if $(product.slib),FRUIT_LIB_STATIC)

