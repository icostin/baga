#ifndef _FRUIT_H
#define _FRUIT_H

#ifdef __cplusplus
extern "C" {
#endif

#if defined(_WIN32)
# define FRUIT_LIB_EXPORT __declspec(dllexport)
# define FRUIT_LIB_IMPORT __declspec(dllimport)
# define FRUIT_LOCAL
#else
# if defined(__GNUC__) && __GNUC__ >= 4
#  define FRUIT_LIB_IMPORT __attribute__ ((visibility ("default")))
#  define FRUIT_LIB_EXPORT __attribute__ ((visibility ("default")))
#  define FRUIT_LOCAL __attribute__ ((visibility ("hidden")))
# else
#  define FRUIT_LIB_IMPORT
#  define FRUIT_LIB_EXPORT
#  define FRUIT_LOCAL
# endif
#endif

#if defined(FRUIT_LIB_STATIC)
# define FRUIT_API FRUIT_LOCAL
#elif defined(FRUIT_LIB_BUILD)
# define FRUIT_API FRUIT_LIB_EXPORT
#else
# define FRUIT_API FRUIT_LIB_IMPORT
#endif

enum fruit_id
{
    FRUIT_ID_APPLE,
    FRUIT_ID_BANANA,
};

typedef struct fruit_s fruit_t;
struct fruit_s
{
    enum fruit_id id_;
};

FRUIT_API char const * fruit_name (fruit_t fruit);

static fruit_t const FRUIT_APPLE = { FRUIT_ID_APPLE };
static fruit_t const FRUIT_BANANA = { FRUIT_ID_BANANA };

#ifdef __cplusplus
}
#endif


#endif /* _FRUIT_H */
