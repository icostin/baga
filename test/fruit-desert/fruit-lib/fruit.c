#include "fruit.h"
#include <plate.h>

/* fruit_name ***************************************************************/
char const * fruit_name (fruit_t fruit)
{
    switch (fruit.id_)
    {
    case FRUIT_ID_APPLE: return "apple";
    case FRUIT_ID_BANANA: return "banana";
    default: 
#if PLATE_DEF < 200
          return "<bad-fruit>";
#else
          return "BAD_FRUIT";
#endif
    }
}

