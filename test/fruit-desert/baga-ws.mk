test := $(notdir $(abspath .))
out_root = ../../_out/$(test)
temp_root = ../../_temp/$(test)

profile_list :=

ifneq ($(host_is_unixlike),)

p := cc-static-debug
profile_list += $p
tags.$p := cc static debug
cpp_includes.$p := plate

p := cc-static-release
profile_list += $p
tags.$p := cc static release strip
cpp_includes.$p := plate

p := cc-dynamic-debug
profile_list += $p
tags.$p := cc dynamic debug
cpp_includes.$p := plate

p := cc-dynamic-release
profile_list += $p
tags.$p := cc dynamic release strip
cpp_includes.$p := plate

p := cc-mini
profile_list += $p
tags.$p := cc static debug quick
project_list.$p := desert
resolve_dep.$p.fruit := fruit-cc-static-release
lib_dep_dirs.$p := $(out_root)/cc-static-release
cpp_includes.$p := plate

endif # }}}

ifneq ($(host_is_windows),)

p := msvc-static-debug
profile_list += $p
tags.$p := msvc static debug windows
cpp_includes.$p := plate

p := msvc-static-release
profile_list += $p
tags.$p := msvc static release windows
cpp_includes.$p := plate

p := msvc-dynamic-debug
profile_list += $p
tags.$p := msvc dynamic debug windows
cpp_includes.$p := plate

p := msvc-dynamic-release
profile_list += $p
tags.$p := msvc dynamic release windows
cpp_includes.$p := plate

endif # }}}
