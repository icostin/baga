#include <stdio.h>

int sum_from_asm (int a, int b);

int main (void)
{
    int a = 8, b = 5, c;
    c = sum_from_asm(a, b);
    printf("%d + %d = %d\n", a, b, c);
    return 0;
}

