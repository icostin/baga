test := $(notdir $(abspath .))
out_root = ../../_out/$(test)
temp_root = ../../_temp/$(test)

profile_list :=

# $1=project $2=profile $3=src
define compile_yasm
$(call src_to_temp_path,$1,$2,$3,$(obj_suffix.$2.$1)): $3 | $(temp_dir.$2.$1)
	$(cmd_pfx)yasm -f $(if $(tag.windows.$2),win64,elf64) -o $$@ $$<

endef

ifneq ($(host_is_unixlike),)

p := amd64-linux_gnu-gcc_yasm
profile_list += $p
tags.$p := cc gcc static release yasm
src_to_obj_rule.yasm.$p = compile_yasm
path.$p := $(PATH):$(HOME)/.local/bin

endif # }}}

ifneq ($(host_is_windows),)

p := amd64-windows-vs_yasm
profile_list += $p
tags.$p := msvc static release yasm windows
src_to_obj_rule.yasm.$p = compile_yasm

endif # }}}

