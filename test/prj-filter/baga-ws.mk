test := $(notdir $(abspath .))
out_root = ../../_out/$(test)
temp_root = ../../_temp/$(test)

profile_list :=

ifneq ($(host_is_unixlike),)

p := cc-static-debug
profile_list += $p
tags.$p := cc static debug

p := cc-static-release
profile_list += $p
tags.$p := cc static release strip

p := cc-dynamic-debug
profile_list += $p
tags.$p := cc dynamic debug

p := cc-dynamic-release
profile_list += $p
tags.$p := cc dynamic release strip

p := cc-mini
profile_list += $p
tags.$p := cc static debug

endif # }}}

ifneq ($(host_is_windows),)

p := msvc-static-debug
profile_list += $p
tags.$p := msvc static debug windows

p := msvc-static-release
profile_list += $p
tags.$p := msvc static release windows

p := msvc-dynamic-debug
profile_list += $p
tags.$p := msvc dynamic debug windows

p := msvc-dynamic-release
profile_list += $p
tags.$p := msvc dynamic release windows

endif # }}}
